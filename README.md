# Transcriptional Orthology

Exploring the relationship between gene expression and sequence gene homologs.

## Goal
* Homolog: a gene inherited in two species by a common ancestor
* Looking human and mouse homologs, are these genes expressed the same way in both species?
* Quantify the similarity and difference of homologs according to expression level data
* This is useful to know for drug testing and other experiments since it could point out genes which behave differently in mice and in humans, rendering certain drugs ineffective or dangerous



## Contents

* **Data Generation.ipynb:**
	* Inputs: a file with rows as GSM experiments and columns as gene entrez ID's for human and one for mouse with the same number of homologous genes (in the same order)
	* Normalize the data and save
	* Compute Pearson and Spearman correlation matrices
* **Data Variance Test.ipynb (DVT):**
	* Inputs: a normalized data file with rows as GSM experiments and columns as gene entrez ID's for either human or mouse
	* Positive control to assess the level of variance present in the data. A baseline so that human and mouse can be compared more effectively
	* Thresholding allows us to ommit the genes with variance levels above an acceptable threshold
* **Human-Human Tests.ipnb:**
	* Inputs: vary depending on the test
	* Space for further experiments involving the human-human data
	* Examples:
		* Pearson or Spearman?
		* Testing the Kneedle Algorithm
		* Finding Knee Points in human gene correlation plots
* **Human-Mouse Analysis.ipynb:**
	* Inputs: A human correlation matrix, a mouse correlation matrix, and the output of the DVT for both human and mouse
	* Compute the correlations betwen each homologous column of the correlation matrices, forming the correlation of correlations
	* Using the kneedle points, compute the correlated and anticorrelated genes for both species' for each gene. Then compute the percent overlap of these and the chi-square score for this overlap

## Examples

**1. Kneedle algorithm and correlated sets:**

Sources:

* [“Finding a ‘Kneedle’ in a Haystack: Detecting Knee Points in System Behavior”](https://raghavan.usc.edu/papers/kneedle-simplex11.pdf)
* [Kneed: Knee point detection in python](https://github.com/arvkevi/kneed)

Description: 

* This useful algorithm finds the knee of a system
* Knee: “a point at which the relative cost to increase some tunable parameter is no longer worth the corresponding performance benefit”
* This is the same as the point of maximum curvature for continuous systems

<img src="./figures/kneedle_diagram.png"/>

* Genes with correlations below the threshold value selected by the algorith would be labeled anticorrelated.
* Then, another knee would be located on the right, and the genes above this threshold value would be labeled correlated

	<img src="./figures/Picture1.png" width="400">

**2. Data Variance Test:**

* For the human-mouse comparison to have relevant results, these correlations should have low variance
* This was tested using the following steps:

1. Determine the number of iterations (I used 10)
2. For each iteration:
	1. Randomly divide the data set in half
	2. Compute correlation matrices for both halves
	3. For each gene:
		1. Compute the correlation between that gene in the first level 1 correlation matrix and that gene in the second level 1 correlation matrix
		2. Using the Kneedle Algorithm, find the percent overlaps between the two correlated sets, anticorrelated sets, and both the anticorrelated and correlated sets together
		3. Also report the size of each set, which is important for interpreting percent overlap

**3. Thresholding:**

* Some genes have significantly lower self-self correlations over multiple random splits than most other genes
* This was dealt with by omitting the genes which had poorer correlations during the Data Variance Test
* Since the genes are homologs between humans and mice, the omitted genes will be the union of the genes below the threshold *t* for human and the genes below threshold *t’* for mice

The steps for thresholding:

1. Select n, the number of thresholds to test (1000)
Compute the ordered list of thresholds, evenly spaced between the minimum correlation value and the maximum correlation
2. For each threshold, compute the number of genes below the threshold
3. Use min-max scaling on both the thresholds and the amounts below the thresholds
4. For each ordered pair, (threshold, amount below) calculate the Euclidean distance from (1,0)
5. Select as the threshold the value with the minimum distance

	<img src="./figures/threshold.png" width="500"/>